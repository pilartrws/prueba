﻿package com.company;

public class Main {

    public static void main(String[] args) {
        try {
            Jugador jugador = new Jugador();
            Equipo equipo = new Equipo();
            equipo.generarEquipos();
            equipo.añadirJugadoresTotal();
            System.out.println("Los cinco mejores jugadores son: \n" + equipo.cincoMejoresJugadores());
            equipo.ELO();
            System.out.println("Los cinco mejores equipos son: \n" + equipo.mejoresCincoEquipos());
            Torneo torneo = new Torneo();
            System.out.println( "Los torneos en función del nivel ELO de cada equipo son: \n" +torneo.torneoJustoEquipos(equipo.getListaTotalEquipos()));

        } catch (Excepcion excepcion) {
            System.out.println(excepcion.getMessage());

        }
    }
}


