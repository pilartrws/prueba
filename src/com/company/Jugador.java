package com.company;

import java.util.*;

class Jugador implements Comparable<Jugador> {
    private String nombre;
    private String apellidos;
    private int rapidez;
    private int ingenio;
    private TipoJugador tipoJugador;


    public Jugador() throws Excepcion {
        this.setNombre(GenerarInfoRandom.nombre());
        this.setApellidos(GenerarInfoRandom.apellido());
        this.setRapidez(GenerarInfoRandom.rapidez());
        this.setIngenio(GenerarInfoRandom.ingenio());
        this.setTipoJugador(GenerarInfoRandom.tipoRandom());

    }


    @Override
    public int compareTo(Jugador jugador) {
        if (this.ELO() < jugador.ELO()) {
            return 1;
        }
        if (this.ELO() > jugador.ELO()) {
            return -1;
        }

        return 0;
    }

    private void setNombre(String nombre) {
        this.nombre = nombre;
    }


    private void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    private void setRapidez(int rapidez) throws Excepcion {
        this.rapidez = rapidez;
        validarRapidez(rapidez);
    }

    private void setIngenio(int ingenio) throws Excepcion {
        this.ingenio = ingenio;
        validarIngenio(ingenio);
    }


    private void setTipoJugador(TipoJugador tipoJugador) {
        this.tipoJugador = tipoJugador;
    }

    protected double ELO() {
        double result = 0;

        switch (tipoJugador) {
            case CARRY:
                result = ingenio * 0.5 + rapidez * 2;
                break;

            case COACH:
                result = ingenio * 3 + rapidez * 0.2;
                break;

        }
        return result;
    }


    protected StringBuffer getJugadorInfo() {
        StringBuffer infoJugador = new StringBuffer();
        infoJugador.append("Nombre: ");
        infoJugador.append(nombre);
        infoJugador.append(" ");
        infoJugador.append(apellidos);
        infoJugador.append("|| ");
        infoJugador.append(" Ingenio: ");
        infoJugador.append(ingenio);
        infoJugador.append("|| ");
        infoJugador.append(" Rapidez ");
        infoJugador.append(rapidez);
        infoJugador.append(" || ");
        infoJugador.append("ELO: ");
        infoJugador.append(ELO());
        return infoJugador;
    }

    private void validarIngenio(int ingenio) throws Excepcion {
        if (ingenio == 0) {
            throw new Excepcion("El ingenio  de un jugador no puede ser 0");
        } else {
            this.ingenio = ingenio;

        }
    }

    private void validarRapidez(int rapidez) throws Excepcion {
        if (rapidez == 0) {
            throw new Excepcion("La rapidez de un jugador no puede ser 0");
        } else {
            this.rapidez = rapidez;

        }
    }

}


