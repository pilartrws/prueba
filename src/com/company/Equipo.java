﻿package com.company;

import java.util.*;


public class Equipo implements Comparable<Equipo> {
    private String nombreEquipo;
    private List<Jugador> listaJugadoresEquipo = new ArrayList<>(5);
    private List<Equipo> listaTotalEquipos = new ArrayList<>(20);
    private List<Jugador> listaJugadoresTotal = new ArrayList<>(100);
    

    protected List<Equipo> getListaTotalEquipos() {
        return listaTotalEquipos;
    }

    private void setListaJugadoresTotal(List<Jugador> listaJugadoresTotal) throws Excepcion {
        this.listaJugadoresTotal = listaJugadoresTotal;
        validarLengthJugadoresTotal(listaJugadoresTotal);
    }

    private void setListaJugadoresEquipo(List<Jugador> listaJugadoresEquipo) throws Excepcion {
        this.listaJugadoresEquipo = listaJugadoresEquipo;

    }

    private List<Jugador> getListaJugadoresEquipo() {
        return listaJugadoresEquipo;
    }

    private void setNombreEquipo(String nombreEquipo) {
        this.nombreEquipo = nombreEquipo;
    }

    protected StringBuffer getEquipoInfo() {
        StringBuffer infoEquipo = new StringBuffer();
        infoEquipo.append(nombreEquipo);
        return infoEquipo;
    }

    @Override
    public int compareTo(Equipo equipo) {
        if (this.ELO() < equipo.ELO()) {
            return 1;
        }
        if (this.ELO() > equipo.ELO()) {
            return -1;
        }

        return 0;
    }

    public Equipo() throws Excepcion {
        this.setNombreEquipo(GenerarInfoRandom.nombreEquipo());
        this.listaJugadoresEquipo = listaJugadoresEquipo;
        validarLengthJugadores(listaJugadoresEquipo);
    }

    protected void generarEquipos() throws Excepcion {

        int contador = 0;


        Equipo equipo[] = new Equipo[20];
        for (int i = 0; i < equipo.length; i++) {
            equipo[i] = new Equipo();
            Jugador jugadores[] = new Jugador[5];
            List<Jugador> aux = new ArrayList<>();
            for (int j = 0; j < jugadores.length; j++) {
                contador++;
                jugadores[j] = new Jugador();
                aux.add(jugadores[j]);
                if (contador == 5) {
                    break;
                }
            }
            equipo[i].setListaJugadoresEquipo(aux);
            listaTotalEquipos.add(equipo[i]);
        }
    }

    protected double ELO() {
        double ELOtot = 0.0;
        int contador = 0;
        for (Jugador jugador : listaJugadoresEquipo) {
            contador++;
            ELOtot += jugador.ELO();
            if (contador == 5) {
                break;
            }
        }
        return ELOtot;
    }


    protected void añadirJugadoresTotal() throws Excepcion {
        for (Equipo equipo : listaTotalEquipos) {
            
               listaJugadoresTotal.addAll(equipo.getListaJugadoresEquipo());

            

        }
        validarLengthJugadoresTotal(listaJugadoresTotal);
    }

    protected StringBuffer mejoresCincoEquipos() {
        StringBuffer equipoInfo = new StringBuffer();
        int contador = 0;
        Collections.sort(listaTotalEquipos);
        for (Equipo equipo : listaTotalEquipos) {
            contador++;
            if (contador <= 5) {
                equipoInfo.append("Nº");
                equipoInfo.append(contador);
                equipoInfo.append("-->");
                equipoInfo.append(equipo.getEquipoInfo());
                equipoInfo.append("||");
                equipoInfo.append("ELO total del equipo:");
                equipoInfo.append(equipo.ELO());
                equipoInfo.append(" \n");
            } else {
                break;
            }
        }
        return equipoInfo;
    }


    protected StringBuffer cincoMejoresJugadores() {
        StringBuffer jugadores = new StringBuffer();
        int contador = 0;
        Collections.sort(listaJugadoresTotal);

        for (Jugador jugador : listaJugadoresTotal) {
            contador++;
            if (contador <= 5) {
                jugadores.append("Nº");
                jugadores.append(contador);
                jugadores.append("-->");
                jugadores.append(jugador.getJugadorInfo());
                jugadores.append(" \n");
            } else {
                break;
            }
        }
        return jugadores;
    }

    private void validarLengthJugadoresTotal(List<Jugador> listaJugadoresTotal) throws Excepcion {
        if (listaJugadoresTotal.size() != 100) {
            throw new Excepcion("El número de jugadores debe ser 100");
        } else {
            this.listaJugadoresTotal = listaJugadoresTotal;

        }
    }

    private void validarLengthEquipo(List<Equipo> listaTotalEquipos) throws Excepcion {
        if (listaTotalEquipos.size() != 100) {
            throw new Excepcion("El número de jugadores debe ser 100");
        } else {
            this.listaTotalEquipos = listaTotalEquipos;

        }

    }

    private void validarLengthJugadores(List<Jugador> listaJugadoresEquipo) throws Excepcion {
        if (listaJugadoresEquipo.size() > 5) {
            throw new Excepcion("El número de jugadores debe ser 5");
        } else {
            this.listaJugadoresEquipo = listaJugadoresEquipo;

        }
    }
}




















