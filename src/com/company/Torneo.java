package com.company;

import java.util.*;

public class Torneo {

    private List<Equipo> lista = new ArrayList<>();

    private boolean esPar(int n) {
        boolean par;
        if (n % 2 == 0) {
            par = true;
        } else {
            par = false;
        }
        return par;
    }

    protected StringBuffer torneoJustoEquipos(List<Equipo> lista) {
        StringBuffer torneos = new StringBuffer();
        int contador = 0;
        Collections.sort(lista);
        for (Equipo equipo : lista) {
            contador++;
            if (!esPar(contador)) {
                torneos.append(equipo.getEquipoInfo());
                torneos.append("||");
                torneos.append("ELO = ");
                torneos.append(equipo.ELO());
            } else {
                torneos.append("--- ");
                torneos.append("VERSUS");
                torneos.append("--- ");
                torneos.append(equipo.getEquipoInfo());
                torneos.append("||");
                torneos.append("ELO = ");
                torneos.append(equipo.ELO());
                torneos.append("\n");
            }

        }
        return torneos;
    }


}








